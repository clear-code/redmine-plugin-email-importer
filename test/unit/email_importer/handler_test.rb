# Copyright (C) 2021  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module EmailImporter
  class HandlerTest < ActiveSupport::TestCase
    def find_issue_options(mail_raw, options)
      mail = Mail.new(mail_raw.b)
      handler = Handler.new
      handler.instance_variable_set(:@email, mail)
      handler.instance_variable_set(:@handler_options, options)
      handler.__send__(:find_issue_options)
    end

    def test_find_issue_options_address_only
      mail = <<-MAIL
From: alice@example.com
To: redmine+test@example.com

Hi
      MAIL
      options = {
        issue_options: {
          "redmine+test@example.com" => {
            "project" => "test",
          },
        }
      }
      assert_equal({"project" => "test"},
                   find_issue_options(mail, options))
    end

    def test_find_issue_options_display_name_and_address
      mail = <<-MAIL
From: alice@example.com
To: "Redmine (test)" <redmine+test@example.com>

Hi
      MAIL
      options = {
        issue_options: {
          "redmine+test@example.com" => {
            "project" => "test",
          },
        }
      }
      assert_equal({"project" => "test"},
                   find_issue_options(mail, options))
    end
  end
end
