# Copyright (C) 2021-2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module EmailImporter
  class BodyCleanerTest < ActiveSupport::TestCase
    def cleanup_body(body)
      cleaner = BodyCleaner.new(body.gsub(/\n/, "\r\n"))
      cleaner.clean
    end

    def test_top_reply_outlook
      body = <<-BODY
Hi,

Great! Thanks!

-----Original Message-----
From: Alice <alice@example.com>
Subject: Hello

Hi Bob!
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_mac_outlook
      body = <<-BODY
Hi,

Great! Thanks!

From: Alice <alice@example.com>
Subject: Hello

Hi Bob!
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_mac_outlook_japanese
      body = <<-BODY
Hi,

Great! Thanks!

送信元: Alice <alice@example.com>
件名: Hello

Hi Bob!
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_from_no_address
      body = <<-BODY
Hi,

Great! Thanks!

From: Alice
Subject: Hello

Hi Bob!
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_hcl_notes_japanese
      body = <<-BODY
Hi,

Great! Thanks!

送信者:\t"Alice" <alice@example.com>
件名:\tHello

Hi Bob!
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_mew
      body = <<-BODY
Hi,

Great! Thanks!

From: "Alice" <alice@example.com>
Subject: Hello
Date: Mon, 30 Aug 2021 14:32:33 +0900 (JST)

> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_mew_custom0
      body = <<-BODY
Hi,

Great! Thanks!

In <alice-message-id@example.com>
  "Hello ..."
  Alice <alice@example.com> wrote:

> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_mirovia_mail
      body = <<-BODY
Hi,

Great! Thanks!

----- Original Message -----
From: Alice <alice@example.com>
Subject: Hello

Hi Bob!
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_mozilla
      body = <<-BODY
Hi,

Great! Thanks!

On 2021/05/15 05:43, Alice wrote:
> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_mutt
      body = <<-BODY
Hi,

Great! Thanks!

On 2021-05-15 05:43, Alice wrote:
> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_gmail_japanese_24_hours
      body = <<-BODY
Hi,

Great! Thanks!

2021年5月15日 5:43 Alice <alice@example.com>:
> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_gmail_japanese_12_hours
      body = <<-BODY
Hi,

Great! Thanks!

2021年5月15日 午前5:43 Alice <alice@example.com>:
> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_gmail_japanese_with_day_of_week
      body = <<-BODY
Hi,

Great! Thanks!

2021年5月15日(土) 5:43 Alice <alice@example.com>:
> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_sylpheed
      body = <<-BODY
Hi,

Great! Thanks!

On Mon, 02 Aug 2021 11:03:42 +0900 (JST)
Alice <alice@example.com> wrote:
> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_top_reply_iphone
      body = <<-BODY
Hi,

Great! Thanks!
________________________________
From: Alice <alice@example.com>

> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

Great! Thanks!
      CLEANED_BODY
    end

    def test_inline_reply_mozilla
      body = <<-BODY
Hi,

On 2021/05/15 05:43, Alice wrote:
> Done!

Great!

Thanks!

> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

On 2021/05/15 05:43, Alice wrote:
> Done!

Great!

Thanks!
      CLEANED_BODY
    end

    def test_inline_reply_mutt
      body = <<-BODY
Hi,

On 2021-05-15 05:43, Alice wrote:
> Done!

Great!

Thanks!

> ...
      BODY
      assert_equal(<<-CLEANED_BODY.chomp, cleanup_body(body))
Hi,

On 2021-05-15 05:43, Alice wrote:
> Done!

Great!

Thanks!
      CLEANED_BODY
    end
  end
end
