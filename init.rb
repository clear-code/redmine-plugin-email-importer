# Copyright (C) 2021-2022  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Redmine::Plugin.register :email_importer do
  name "Email importer plugin"
  author "Sutou Kouhei"
  description "This is a Redmine plugin to import email"
  version "1.0.0"
  url "https://gitlab.com/redmine-plugin-email-importer/redmine-plugin-email-importer"
  author_url "https://gitlab.com/redmine-plugin-email-importer"
  directory __dir__
end

Rails.application.config.x.email_importer =
  Rails.application.config_for(:email_importer)

prepare = lambda do
  EmailImporter::BodyCleaner
  EmailImporter::Handler
  EmailImporter::Importer
end

# We need to initialize explicitly with Redmine 5.0 or later.
prepare.call if Redmine.const_defined?(:PluginLoader)

Rails.configuration.to_prepare(&prepare)
