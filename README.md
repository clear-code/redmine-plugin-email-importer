# Redmine plugin email importer

A Redmine plugin to update issues via email.

This is similar to the official `redmine:email:receive_imap` but this
works without Redmine related information such as `<redmine...>`
Message-Id and `[#ISSUE_ID]` mark in Subject.

## Install

Install this plugin:

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-email-importer/redmine-plugin-email-importer.git plugins/email_importer
```

Restart Redmine.

### Register periodical task

You need to run `email_importer:import` task periodically:

```bash
cd redmine
RAILS_ENV=production plugins/email_importer/bin/generate_systemd_service | \
  sudo -H tee /lib/systemd/system/redmine-email-importer.service
RAILS_ENV=production plugins/email_importer/bin/generate_systemd_timer | \
  sudo -H tee /lib/systemd/system/redmine-email-importer.timer
sudo -H systemctl daemon-reload
sudo -H systemctl enable --now redmine-email-importer.timer
sudo -H systemctl start redmine-email-importer.service
```

## Uninstall

Uninstall systemd service:

```bash
sudo -H systemctl disable --now redmine-email-importer.timer
sudo -H rm /lib/systemd/system/redmine-email-importer.timer
sudo -H rm /lib/systemd/system/redmine-email-importer.service
sudo -H systemctl daemon-reload
```

Uninstall this plugin:

```bash
cd redmine
rm -rf plugins/email_importer
```

Restart Redmine.

## Usage

Specify your environment in `config/email_importer.yml`. You can use
`plugins/email_importer/config/email_importer.yml.example` as a base
file. See the example file how to configure.

## Authors

  * Sutou Kouhei `<kou@clear-code.com>`

## License

GPLv2 or later. See [LICENSE](LICENSE) for details.

