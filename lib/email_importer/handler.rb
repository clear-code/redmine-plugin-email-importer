# Copyright (C) 2021-2023  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module EmailImporter
  class Handler < MailHandler
    def receive(email, options={})
      if email.from.is_a?(String)
        email.from = extract_email(email.from)
      end
      super(email, options)
    end

    private
    def extract_email(address)
      stripped_address = address.strip

      # "XXX <xxx@example.com>"
      email = stripped_address[/<(.+?)>\z/, 1]
      return email if email

      # "<XXX>xxx@example.com"
      email_pattern_no_position =
        URI::MailTo::EMAIL_REGEXP.to_s.gsub(/\\A|\\z/, "")
      email = stripped_address[/#{email_pattern_no_position}/, 0]
      return email if email

      address
    end

    def detect_user
      return unless @user.anonymous?

      github_id_custom_field_id = @handler_options[:github_id_custom_field_id]
      return if github_id_custom_field_id.nil?

      github_user_id = @email["X-GitHub-Sender"]&.field&.value
      return if github_user_id.nil?

      user = User
               .joins(:custom_values)
               .where(custom_values: {
                        custom_field_id: github_id_custom_field_id,
                        value: github_user_id,
                      })
               .first
      return if user.nil?
      return unless user.active?

      @user = user
      User.current = @user
    end

    def dispatch
      message_ids_custom_field_id =
        @handler_options[:message_ids_custom_field_id]
      github_custom_field_id = @handler_options[:github_custom_field_id]
      detect_user
      references = @email.references || []
      references = [references] if references.is_a?(String)
      if references.empty?
        issues = []
      else
        value_conditions = []
        value_condition_values = []
        references.each do |reference|
          value_conditions << Redmine::Database.like("custom_values.value", "?")
          value_condition_values << "%<#{reference}>%"
        end
        issues =
          Issue.
            where(project: Project.active).
            joins(:custom_values).
            where(custom_values: {custom_field_id: message_ids_custom_field_id}).
            where(value_conditions.join(" OR "),
                  *value_condition_values).
            to_a
      end

      if issues.empty?
        project = target_project
        issues << receive_issue if project
      else
        issues.each do |issue|
          receive_issue_reply(issue.id)
        end
      end
      issues.each do |issue|
        issue.reload
        journal = issue.init_journal(user)

        if handler_options[:no_permission_check]
          attributes = issue_attributes_from_keywords(issue)
          status_id = attributes["status_id"]
          issue.status_id = status_id if status_id.present?
        end

        issue.custom_values.each do |custom_value|
          case custom_value.custom_field_id
          when message_ids_custom_field_id
            message_ids =
              custom_value.value.split |
              (references.collect {|r| "<#{r}>"})
            if @email.message_id.present?
              message_ids |= ["<#{@email.message_id}>"]
            end
            custom_value.value = message_ids.uniq.sort.join("\r\n")
            custom_value.save
          when github_custom_field_id
            case @email.message_id.to_s
            when /\A([a-zA-Z0-9_-]+\/[a-zA-Z0-9_-]+)
                    \/
                    (?:pull|issues|repo-discussions)
                    \/
                    (\d+)
                    (?:\/[^@]+)?
                    @github\.com\z/x
              repository = $1
              id = $2
              custom_value.value = "#{repository}##{id}"
              custom_value.save
            end
          end
        end

        issue.save! if issue.changed?
      end
      true
    end

    def option_value(options, key)
      if options.keys.first.is_a?(Symbol)
        options[key]
      else
        options[key.to_s]
      end
    end

    def find_issue_options
      issue_options = @handler_options[:issue_options]
      [:to, :cc, :bcc].each do |field|
        header = @email[field]
        field = header&.field
        next if field.nil?
        next unless field.respond_to?(:addrs)
        field.addrs.each do |addr|
          options = option_value(issue_options, addr.address.downcase.to_sym)
          return options if options
        end
      end
      nil
    end

    def target_project
      options = find_issue_options
      if options
        project = Project.find_by_identifier(option_value(options, :project))
        return project if project
        subject_tags = option_value(options, :subject_tags) || {}
        subject_tags.each do |subject_tag, suboptions|
          next unless (@email.subject || "").start_with?(subject_tag)
          project = Project.find_by_identifier(option_value(suboptions, :project))
          return project if project
        end
      end
      nil
    end

    def get_keyword(attr, options={})
      value = super
      return value if value

      header_value = @email["X-Redmine-#{attr.to_s.classify}"]
      if header_value
        @keywords[attr] = header_value
        return header_value
      end

      issue_options = find_issue_options
      if issue_options.nil?
        @keywords[attr] = nil
      else
        @keywords[attr] = option_value(issue_options, attr)
      end
      @keywords[attr]
    end

    def add_watchers(obj)
      # Disabled
    end

    def generate_keywords_from_body(body)
      keywords = []
      close_status_name = @handler_options[:close_status_name]
      return keywords if close_status_name.nil?

      if @email["X-GitHub-Sender"]
        generate_keywords_from_body_github(body, keywords, close_status_name)
      end
      keywords
    end

    def generate_keywords_from_body_github(body, keywords, close_status_name)
      normalized_body = body.gsub(/\#\d+/, "#XXX").chomp
      return if normalized_body.lines.size != 1

      case normalized_body
      when "Closed \#XXX.",
           "Closed \#XXX as not planned.",
           "Closed \#XXX as completed.",
           /\AClosed \#XXX as completed via \h{40}\.\z/,
           /\AClosed \#XXX via \h{40}\.\z/,
           "Closed \#XXX as resolved.",
           /\AMerged \#XXX into .+\.\z/
        key = l("field_status", locale: Setting.default_language)
        keywords << "#{key}: #{close_status_name}"
      end
    end

    def cleanup_body(body)
      cleaner = BodyCleaner.new(super)
      body = cleaner.clean
      keywords = generate_keywords_from_body(body)
      <<-MARKDOWN
#{keywords.join("\n")}
`````text
#{body}
`````
      MARKDOWN
    end
  end
end
