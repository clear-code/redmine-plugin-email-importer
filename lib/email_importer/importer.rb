# Copyright (C) 2021-2023  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require "net/imap"

module EmailImporter
  class Importer
    def initialize
      # Ensure loading AnonymousUser for issues that aren't associated
      # with any user.
      AnonymousUser
      @config = Rails.application.config.x.email_importer
    end

    def import
      message_ids_custom_field = IssueCustomField.find_by(name: "Message-Ids",
                                                          is_filter: true)
      return if message_ids_custom_field.nil?

      github_custom_field = IssueCustomField.find_by(name: "GitHub")
      github_id_custom_field = UserCustomField.find_by(name: "GitHub ID")
      first_close_status = IssueStatus.where(is_closed: true).order(:position).first

      imap = Net::IMAP.new(config_value(:imap, :address),
                           config_value(:imap, :port))
      if config_value(:imap, :start_tls)
        imap.starttls
      end
      imap.login(config_value(:imap, :user_name),
                 config_value(:imap, :password))
      imap.select(config_value(:imap, :inbox) || "INBOX")
      handler_options = {
        allow_override: ["status"],
        close_status_name: first_close_status&.name,
        github_custom_field_id: github_custom_field&.id,
        github_id_custom_field_id: github_id_custom_field&.id,
        issue_options: config_value(:issue) || {},
        message_ids_custom_field_id: message_ids_custom_field.id,
        no_account_notice: "1",
        no_notification: "1",
        no_permission_check: "1",
        unknown_user: "accept",
      }
      trash = config_value(:imap, :trash) || "Trash"
      imap.uid_search(["NOT", "SEEN"]).each do |uid|
        message = imap.uid_fetch(uid, "RFC822")[0].attr["RFC822"]
        if Handler.receive(message, handler_options)
          imap.uid_copy(uid, trash)
          imap.uid_store(uid, "+FLAGS", [:Seen, :Deleted])
        else
          imap.uid_store(uid, "+FLAGS", [:Seen])
        end
      end
      imap.expunge
      imap.select(trash)
      trash_keep_days = config_value(:process, :trash_keep_days) || 30
      before = Time.zone.now.ago(trash_keep_days.days)
      imap.uid_search(["BEFORE", before.strftime("%d-%b-%Y")]).each do |uid|
        imap.uid_store(uid, "+FLAGS", [:Deleted])
      end
      imap.expunge
      imap.logout
      imap.disconnect
    end

    private
    def config_value(*keys)
      if @config.keys.first.is_a?(Symbol)
        @config.dig(*keys)
      else
        @config.dig(*(keys.collect(&:to_s)))
      end
    end
  end
end
