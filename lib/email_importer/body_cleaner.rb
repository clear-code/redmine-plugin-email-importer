# Copyright (C) 2021-2023  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module EmailImporter
  class BodyCleaner
    def initialize(body)
      @body = body
    end

    def clean
      body = normalize_new_line(@body)
      body, additional_part = remove_signature(body)
      top_part, reply_part = split_reply_part(body)
      if reply_part.lines.all? {|line| reply_line?(line)}
        body = top_part
      end
      body = remove_last_reply_only_block(body)
      [body.strip, additional_part].compact.join("\n\n")
    end

    def normalize_new_line(text)
      text.gsub(/\r\n/, "\n")
    end

    def remove_signature(text)
      body, signature = text.split(/^-- $/, 2)
      if signature
        auto_added_marker = "*" * 47
        auto_added_marker_pattern = Regexp.escape(auto_added_marker)
        auto_added_text = signature[/^#{auto_added_marker_pattern}$
                                     .*
                                     ^#{auto_added_marker_pattern}\Z/mx]
        additional_part = auto_added_text&.strip
      else
        additional_part = nil
      end
      [body, additional_part]
    end

    def split_reply_part(text)
      in_header = true
      status = :top
      reply_variant = nil
      top_part = ""
      reply_part = ""
      last_original_header = nil
      text.each_line do |line|
        stripped_line = line.strip
        case status
        when :top
          case stripped_line
          when "-----Original Message-----"
            # Outlook
            # top-reply is only supported
            break
          when /\A送信元: .*<.*>\z/
            # MacOutlook: Japanese
            # top-reply is only supported
            status = :original_headers
            last_original_header = stripped_line
          when /\AFrom: .*(?:<.*>)?\z/
            # Outlook, Mew and others
            # Some "From: ..." style mailers may not add e-mail address.
            status = :original_headers
            last_original_header = stripped_line
          when /\A送信者:\s+.*<.*>\z/
            # HCL Notes: Japanese
            status = :original_headers
            last_original_header = stripped_line
          when /\AIn <.*>\z/
            # Mew: Custom0
            status = :reply_mark
            reply_variant = :mew_custom0
          when "----- Original Message -----"
            # Mirovia Mail
            # top-reply is only supported
            break
          when /\AOn \d{4}\/\d{2}\/\d{2} .* wrote:\z/
            # Mozilla
            status = :reply
          when /\AOn \d{4}-\d{2}-\d{2} .* wrote:\z/
            # Mutt
            status = :reply
          when /\A\d{4}年\d{1,2}月\d{1,2}日(?:\(.\))? (?:午前|午後)?\d{1,2}:\d{1,2} .*:\z/
            # Gmail: Japanese
            status = :reply
          when /\AOn [A-Z][a-z]{2}, \d{2} [A-Z][a-z]{2} \d{4} \d{2}:\d{2}:\d{2} [+-]\d{4} \([A-Z]{3}\)\z/
            # Sylpheed
            status = :reply_mark
            reply_variant = :sylpheed
          when /\A_{10,}\z/
            # iPhone
            # top-reply is only supported
            break
          else
            if in_header
              if stripped_line.empty?
                in_header = false
              else
                last_original_header = stripped_line
              end
            end
            top_part << line
          end
        when :original_headers
          if stripped_line.empty?
            case last_original_header
            when /\ADate:/
              # Mew
              status = :reply_mark
              reply_variant = :mew
            else
              # Outlook
              break
            end
          else
            last_original_header = stripped_line
          end
        when :reply_mark
          case reply_variant
          when :mew, :mew_custom0
            if stripped_line.empty?
              status = :reply
            end
          when :sylpheed
            case stripped_line
            when /\A.*<.+?> wrote:\z/
              status = :reply
            else
              status = :top
            end
          end
        when :reply
          reply_part << line
        end
      end
      [top_part, reply_part]
    end

    def remove_last_reply_only_block(text)
      lines = text.lines
      loop do
        break if lines.empty?
        last_line = lines[-1]
        break unless reply_line?(last_line)
        lines.pop
      end
      lines.join
    end

    def reply_line?(line)
      line = line.strip
      line.empty? or line.start_with?(">")
    end
  end
end
